package com.accounts;

import java.sql.*;

public class AccountApplication {

    private static String dbURL = "jdbc:derby:accountDB;create=true;user=APP;password=";
    private static String tableName = "T_ACCOUNT";
    private static String tableName2 = "T_TRANSACTION";
    private static String tableName3 = "T_OPERATION";
    // jdbc Connection
    private static Connection conn = null;
    private static Statement stmt = null;

    public static void main(String[] args)
    {
        createConnection();
        insertAccounts(24891, "Alex O`brien", "Deposit");
        insertAccounts(56732, "Brigitte Meyer", "Funds");
        selectAccounts();
        insertTransactions(186, "DEP", "Deposit");
        insertTransactions(357, "TRA", "Transfer");
        insertTransactions(792, "WIT", "Withdrawal");
        selectTransactions();
        insertOperations(56732,"DEP", 100);
        selectOperations();
        shutdown();
    }

    private static void createConnection()
    {
        try
        {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            //Get a connection
            conn = DriverManager.getConnection(dbURL);
        }
        catch (Exception except)
        {
            except.printStackTrace();
        }
    }

    private static void insertAccounts(int accountNumber, String customerName, String accountType)
    {
        try
        {
            stmt = conn.createStatement();

            if (!doesTableExists(tableName, conn)) {
                String sql = "CREATE TABLE " + tableName +  " (accountNumber int, customerName varchar(255), accountType varchar(255))";
                stmt.execute(sql);
                //System.out.println("Created table " + tableName + ".");
            }

            String sql = "INSERT INTO " + tableName + " VALUES (" + accountNumber + ", '" + customerName + "', '" + accountType + "')";
            stmt.execute(sql);
            //System.out.println("Inserted row.");
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void selectAccounts()
    {
        try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("select * from " + tableName);
            ResultSetMetaData rsmd = results.getMetaData();
            int numberCols = rsmd.getColumnCount();
            System.out.println("\n");
            for (int i=1; i<=numberCols; i++)
            {
                //print Column Names
                System.out.print(rsmd.getColumnLabel(i)+"\t\t");
            }

            System.out.println("\n---------------------------------------------------------------");

            while(results.next())
            {
                int accountNumber = results.getInt(1);
                String customerName = results.getString(2);
                String accountType = results.getString(3);
                System.out.println(accountNumber + "\t\t" + customerName + "\t\t" + accountType);
            }
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void insertTransactions(int transactionId, String transactionCode, String transactionDescription)
    {
        try
        {
            stmt = conn.createStatement();

            if (!doesTableExists(tableName2, conn)) {
                String sql = "CREATE TABLE " + tableName2 +  " (transactionId int, transactionCode varchar(255), transactionDescription varchar(255))";
                stmt.execute(sql);
                //System.out.println("Created table " + tableName2 + ".");
            }

            String sql = "INSERT INTO " + tableName2 + " VALUES (" + transactionId + ", '" + transactionCode + "', '" + transactionDescription + "')";
            stmt.execute(sql);
            //System.out.println("Inserted row.");
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void selectTransactions()
    {
        try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("select * from " + tableName2);
            ResultSetMetaData rsmd = results.getMetaData();
            int numberCols = rsmd.getColumnCount();
            System.out.println("\n");
            for (int i=1; i<=numberCols; i++)
            {
                //print Column Names
                System.out.print(rsmd.getColumnLabel(i)+"\t\t");
            }

            System.out.println("\n----------------------------------------------------------------");

            while(results.next())
            {
                int transactionId = results.getInt(1);
                String transactionCode = results.getString(2);
                String transactionDescription = results.getString(3);
                System.out.println(transactionId + "\t\t" + transactionCode + "\t\t" + transactionDescription);
            }
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void insertOperations(int accountNumber, String transactionCode, double currencyAmountInPounds)
    {
        try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("select * from " + tableName);
            while(results.next())
            {
                if (results.getInt(1) == accountNumber) {
                    break;
                }
            }

            results = stmt.executeQuery("select * from " + tableName2);
            while(results.next())
            {
                if (results.getString(2).equals(transactionCode)) {
                    break;
                }else {throw new java.lang.Error("Have No or Wrong Transaction Code");}
            }

            float currencyAmountInEuros = (float) (currencyAmountInPounds * 1.10);

            if (!doesTableExists(tableName3, conn)) {
                String sql = "CREATE TABLE " + tableName3 +  " (accountNumber int, transactionCode varchar(255), currencyAmountInEuros Double)";
                stmt.execute(sql);
            }

            String sql = "INSERT INTO " + tableName3 + " VALUES (" + accountNumber + ", '" + transactionCode + "', " + currencyAmountInEuros + ")";
            stmt.execute(sql);
            stmt.close();

        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void selectOperations()
    {
        try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("select * from " + tableName3);
            ResultSetMetaData rsmd = results.getMetaData();
            int numberCols = rsmd.getColumnCount();
            System.out.println("\n");
            System.out.println("OPERATIONS");
            System.out.println("**********");
            for (int i=1; i<=numberCols; i++)
            {
                //print Column Names
                System.out.print(rsmd.getColumnLabel(i)+"\t\t");
            }

            System.out.println("\n----------------------------------------------------------------");

            while(results.next())
            {
                int accountNumber = results.getInt(1);
                String transactionCode = results.getString(2);
                Double currencyAmountInEuros = results.getDouble(3);
                System.out.println(accountNumber + "\t\t" + transactionCode + "\t\t" + currencyAmountInEuros);
            }
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }

    private static void shutdown()
    {
        try
        {
            if (stmt != null)
            {
                stmt.close();
            }
            if (conn != null)
            {
                DriverManager.getConnection(dbURL + ";shutdown=true");
                conn.close();
            }
        }
        catch (SQLException sqlExcept)
        {

        }
    }

    private static boolean doesTableExists(String tableName, Connection conn)
            throws SQLException {
        DatabaseMetaData meta = conn.getMetaData();
        ResultSet result = meta.getTables(null, null, tableName.toUpperCase(), null);

        return result.next();
    }
}

